defmodule Tist.AuthorizationTest do
  use Tist.DataCase

  alias Tist.Authorization
  alias Tist.Admin
  alias Tist.Registration

  describe "users" do
    @valid_attrs %{login: "some login", name: "some name", password: "some password_hash"}

    def user_fixture do
      {:ok, user} = Registration.create(@valid_attrs)
      {:ok, user} = Admin.approve_user(user.id)

      user
    end

    test "authenticate_user/2 returns user with a given login and password" do
      user = user_fixture()

      assert Authorization.authenticate_user(%{"login" => "some login", "password" => "some password_hash"}) == {:ok, Authorization.get_user!(user.id)}
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Authorization.get_user!(user.id).login == "some login"
    end
  end
end
