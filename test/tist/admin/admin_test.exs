defmodule Tist.AdminTest do
  use Tist.DataCase

  alias Tist.Admin

  describe "users" do
    alias Tist.Admin.User

    def user_fixture do
      Tist.Registration.create(%{login: "L", name: "N", password: "password"})
    end

    test "list_users/0 returns all users" do
      {:ok, user} = user_fixture()
      assert Admin.list_users() == [Repo.get(User, user.id)]
    end

    test "delete_user/1 deletes the user" do
      {:ok, user} = user_fixture()
      assert {:ok, %User{}} = Admin.delete_user(user.id)
      assert_raise Ecto.NoResultsError, fn -> Repo.get!(Admin.User, user.id) end
    end

    test "approve_user, approves a user and removes it from pending users" do
      {:ok, user } = user_fixture()

      assert {:ok, %User{approved: true}} = Admin.approve_user(user.id)
    end
  end
end
