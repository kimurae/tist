defmodule Tist.TwilightImperiumTest do
  use Tist.DataCase

  alias Tist.TwilightImperium

  describe "twilight_imperium_games" do
    alias Tist.TwilightImperium.{Faction, Game}

    def faction_fixture do
      %Faction{name: "test"}
      |> Tist.Repo.insert!
    end

    def valid_attrs do
      %{
        played_on: ~D[2010-04-17],
        player_count: 3,
        turns: 42,
        winning_faction_id: faction_fixture().id
      }
    end

    @update_attrs %{played_on: ~D[2011-05-18], turns: 43}
    @invalid_attrs %{played_on: nil, turns: nil, player_count: 2, winning_faction_id: nil}

    def game_fixture(user, attrs \\ %{}) do
      {:ok, game} =
        TwilightImperium.create_game(user.id, Enum.into(attrs, valid_attrs))

      game
    end

    test "list_twilight_imperium_games/0 returns all twilight_imperium_games" do
      user = current_user_fixture()
      game_fixture(user)
      assert Enum.count(TwilightImperium.list_twilight_imperium_games(user.id)) == 1
    end

    test "get_game!/1 returns the game with given id" do
      user = current_user_fixture()
      game = game_fixture(user)
      assert TwilightImperium.get_game!(user.id, game.id) == game
    end

    test "create_game/1 with valid data creates a game" do
      user = current_user_fixture()

      assert {:ok, %Game{} = game} = TwilightImperium.create_game(user.id, valid_attrs())

      assert game.played_on     == ~D[2010-04-17]
      assert game.player_count  == 3
      assert game.turns         == 42
    end

    test "create_game/1 with invalid data returns error changeset" do
      user = current_user_fixture()
      assert {:error, %Ecto.Changeset{}} = TwilightImperium.create_game(user.id, @invalid_attrs)
    end

    test "update_game/2 with valid data updates the game" do
      user = current_user_fixture()
      game = game_fixture(user)

      assert {:ok, game}    = TwilightImperium.update_game(game, @update_attrs)
      assert %Game{}        = game
      assert game.played_on == ~D[2011-05-18]
      assert game.turns     == 43
      assert game.user_id   == user.id
    end

    test "update_game/2 with invalid data returns error changeset" do
      user = current_user_fixture()
      game = game_fixture(user)

      assert {:error, %Ecto.Changeset{}} = TwilightImperium.update_game(game, @invalid_attrs)

      assert game == TwilightImperium.get_game!(user.id, game.id)
    end

    test "delete_game/1 deletes the game" do
      user = current_user_fixture()
      game = game_fixture(user)
      assert {:ok, %Game{}} = TwilightImperium.delete_game(game)

      assert_raise Ecto.NoResultsError, fn -> TwilightImperium.get_game!(user.id, game.id) end
    end

    test "change_game/1 returns a game changeset" do
      user = current_user_fixture()
      game = game_fixture(user)

      assert %Ecto.Changeset{} = TwilightImperium.change_game(game)
    end
  end

  describe "twilight_imperium_scores" do
    alias Tist.TwilightImperium.{Faction, Game, Score}

    def score_valid_attrs do
      %{
        faction_id: faction_fixture().id,
        score: 43
      }
    end

    @update_attrs   %{score: 1}
    @invalid_attrs  %{faction: nil, score: nil}

    def game_fixture_for_score(user, attrs \\ %{}) do
      {:ok, game} =
        TwilightImperium.create_game(user.id, Enum.into(attrs, %{
          played_on: ~D[2010-04-17],
          player_count: 3,
          turns: 42,
          winning_faction_id: faction_fixture().id
        }))

      game
    end

    def score_fixture(game, attrs \\ %{}) do
      {:ok, score} = TwilightImperium.create_score(game, Enum.into(attrs, score_valid_attrs()))

      score
    end

    test "get_score!/2 returns the game with given id" do
      game = current_user_fixture()
        |> game_fixture_for_score()

      score = score_fixture(game)
      assert TwilightImperium.get_score!(game, score.id) == score
    end

    test "create_score/2 with valid data creates a game" do
      game = current_user_fixture()
        |> game_fixture_for_score()

      assert {:ok, %Score{} = score} = TwilightImperium.create_score(game, score_valid_attrs())

      assert score.score == 43
    end

    test "create_score/2 with invalid data returns error changeset" do
      game = current_user_fixture()
        |> game_fixture_for_score()
      assert {:error, %Ecto.Changeset{}} = TwilightImperium.create_score(game, @invalid_attrs)
    end

    test "update_score/2 with valid data updates the score" do
      game = current_user_fixture()
        |> game_fixture_for_score()

      score = score_fixture(game)

      assert {:ok, score}    = TwilightImperium.update_score(score, @update_attrs)
      assert %Score{}        = score
      assert score.score     == 1
      assert score.game_id   == game.id
    end

    test "update_score/2 with invalid data returns error changeset" do
      game = current_user_fixture()
        |> game_fixture_for_score()

      score = score_fixture(game)

      assert {:error, %Ecto.Changeset{}} = TwilightImperium.update_score(score, @invalid_attrs)

      assert score == TwilightImperium.get_score!(game, score.id)
    end

    test "delete_score/1 deletes the score" do
      game = current_user_fixture()
        |> game_fixture_for_score()

      score = score_fixture(game)

      assert {:ok, %Score{}} = TwilightImperium.delete_score(score)

      assert_raise Ecto.NoResultsError, fn -> TwilightImperium.get_score!(game, score.id) end
    end

    test "change_score/1 returns a score changeset" do
      score = current_user_fixture()
        |> game_fixture_for_score()
        |> score_fixture()

      assert %Ecto.Changeset{} = TwilightImperium.change_score(score)
    end
  end
end
