defmodule Tist.RegistrationTest do
  use Tist.DataCase

  alias Tist.Registration
  alias Comeonin.Bcrypt

  describe "users" do
    alias Tist.Registration.User

    @valid_attrs %{login: "some login", name: "some name", password: "some password_hash"}
    @invalid_attrs %{login: nil, name: nil, password_hash: nil}

    test "create/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Registration.create(@valid_attrs)

      assert user.login         == "some login"
      assert user.name          == "some name"
      assert user.password      == "some password_hash"

      assert Bcrypt.checkpw("some password_hash", user.password_hash)
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Registration.create(@invalid_attrs)
    end

    test "create/1 with invalid duplicate returns error changeset" do
      Registration.create(@valid_attrs)

      assert {:error, %Ecto.Changeset{}} = Registration.create(%{login: "some login", name: "another name", password: "some password_hash"})
      assert {:error, %Ecto.Changeset{}} = Registration.create(%{login: "antother login", name: "some name", password: "some password_hash"})
    end

    test "changeset/1 returns a user changeset" do
      assert %Ecto.Changeset{} = Registration.changeset()
    end
  end
end
