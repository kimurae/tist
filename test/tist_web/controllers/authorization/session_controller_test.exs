defmodule TistWeb.Authorization.SessionControllerTest do
  use TistWeb.ConnCase

  alias Tist.Admin
  alias Tist.Registration

  @create_attrs %{login: "some login", name: "some name", password: "some password_hash"}

  def fixture(:user) do
    {:ok, user} = Registration.create(@create_attrs)
    {:ok, user} = Admin.approve_user(user.id)

    user
  end

  describe "new session" do
    test "renders form", %{conn: conn} do
      conn = get conn, authorization_session_path(conn, :new)
      assert html_response(conn, 200) =~ "Login"
    end
  end

  describe "create session" do
    setup [:create_user]

    test "redirects to root when data is valid", %{conn: conn} do
      conn = post conn, authorization_session_path(conn, :create), session: %{login: "some login", password: "some password_hash"}

      assert redirected_to(conn) == "/"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, authorization_session_path(conn, :create), session: %{login: "your mom", password: "meh"}
      assert html_response(conn, 200) =~ "Login"
    end
  end

  describe "delete session" do
    setup [:create_user, :login_user]

    test "deletes chosen session", %{conn: conn, user: user} do
      conn = delete conn, authorization_session_path(conn, :delete, user)

      assert redirected_to(conn) == "/"
    end
  end

  defp create_user(_) do
    user = fixture(:user)
    {:ok, user: user}
  end

  defp login_user(_), do: login(false)
end
