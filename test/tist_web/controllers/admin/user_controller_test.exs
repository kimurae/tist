defmodule TistWeb.Admin.UserControllerTest do
  use TistWeb.ConnCase

  setup do
    login(true)
  end

  def fixture(:user) do
    {:ok, user} = Tist.Registration.create(%{login: "A", name: "N", password: "password"})
    user
  end

  describe "index" do
    test "lists all users", %{conn: conn} do
      conn = get conn, admin_user_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Users"
    end
  end

  describe "update user" do
    setup [:register_user]

    test "redirects when data is valid", %{conn: conn, user: user} do
      conn = put conn, admin_user_path(conn, :update, user)
      assert redirected_to(conn) == admin_user_path(conn, :index)
    end
  end

  describe "delete user" do
    setup [:register_user]

    test "deletes chosen user", %{conn: conn, user: user} do
      conn = delete conn, admin_user_path(conn, :delete, user)
      assert redirected_to(conn) == admin_user_path(conn, :index)
    end
  end

  defp register_user(_) do
    user = fixture(:user)
    {:ok, user: user}
  end
end
