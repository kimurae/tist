defmodule TistWeb.TwilightImperium.GameControllerTest do
  use TistWeb.ConnCase

  alias Tist.TwilightImperium

  @update_attrs %{played_on: ~D[2011-05-18], turns: 43}
  @invalid_attrs %{played_on: nil, turns: nil}

  def create_attrs do
    winning_faction = Tist.Repo.insert!(%TwilightImperium.Faction{name: "Test"})

    %{played_on: ~D[2010-04-17], player_count: 3, turns: 42, winning_faction_id: winning_faction.id}
  end

  def fixture(:game, user_id) do
    {:ok, game} = TwilightImperium.create_game(user_id, create_attrs())
    game
  end

  describe "index" do
    setup [:login_user]

    test "lists all twilight_imperium_games", %{conn: conn} do
      conn = get conn, twilight_imperium_game_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Twilight imperium games"
    end
  end

  describe "new game" do
    setup [:login_user]

    test "renders form", %{conn: conn} do
      conn = get conn, twilight_imperium_game_path(conn, :new)
      assert html_response(conn, 200) =~ "New Game"
    end
  end

  describe "create game" do
    setup [:login_user]

    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, twilight_imperium_game_path(conn, :create), game: create_attrs()

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == twilight_imperium_game_path(conn, :show, id)

      conn = get conn, twilight_imperium_game_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Game"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, twilight_imperium_game_path(conn, :create), game: @invalid_attrs
      assert html_response(conn, 200) =~ "New Game"
    end
  end

  describe "edit game" do
    setup [:login_user, :create_game]

    test "renders form for editing chosen game", %{conn: conn, game: game} do
      conn = get conn, twilight_imperium_game_path(conn, :edit, game)
      assert html_response(conn, 200) =~ "Edit Game"
    end
  end

  describe "update game" do
    setup [:login_user, :create_game]

    test "redirects when data is valid", %{conn: conn, game: game} do
      conn = put conn, twilight_imperium_game_path(conn, :update, game), game: @update_attrs
      assert redirected_to(conn) == twilight_imperium_game_path(conn, :show, game)

      conn = get conn, twilight_imperium_game_path(conn, :show, game)
      assert html_response(conn, 200)
    end

    test "renders errors when data is invalid", %{conn: conn, game: game} do
      conn = put conn, twilight_imperium_game_path(conn, :update, game), game: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Game"
    end
  end

  describe "delete game" do
    setup [:login_user, :create_game]

    test "deletes chosen game", %{conn: conn, game: game} do
      conn = delete conn, twilight_imperium_game_path(conn, :delete, game)
      assert redirected_to(conn) == twilight_imperium_game_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, twilight_imperium_game_path(conn, :show, game)
      end
    end
  end

  defp create_game(%{conn: conn}) do
    game = fixture(:game, current_user(conn).id)
    {:ok, game: game}
  end

  defp login_user(_) do
    login(false)
  end
end
