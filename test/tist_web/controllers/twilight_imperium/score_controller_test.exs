defmodule TistWeb.TwilightImperium.ScoreControllerTest do
  use TistWeb.ConnCase

  alias Tist.TwilightImperium

  @update_attrs %{score: 1}
  @invalid_attrs %{faction_id: nil, score: nil}

  def create_attrs do
    faction = Tist.Repo.insert!(%TwilightImperium.Faction{name: "Test 2"})

    %{faction_id: faction.id, score: 42}
  end

  def fixture(:game, user_id) do
    winning_faction = Tist.Repo.insert!(%TwilightImperium.Faction{name: "Test"})

    {:ok, game} = TwilightImperium.create_game(user_id, %{
      played_on:    ~D[2010-04-17],
      player_count: 3,
      turns:        42,
      winning_faction_id: winning_faction.id
    })

    game
  end

  def fixture(:score, game) do
    {:ok, score} = TwilightImperium.create_score(game, create_attrs())
    score
  end

  describe "new score" do
    setup [:login_user, :create_game]

    test "renders form", %{conn: conn, game: game} do
      conn = get conn, twilight_imperium_game_score_path(conn, :new, game)
      assert html_response(conn, 200) =~ "New Victory Point Score"
    end
  end

  describe "create score" do
    setup [:login_user, :create_game]

    test "redirects to show when data is valid", %{conn: conn, game: game} do
      conn = post conn, twilight_imperium_game_score_path(conn, :create, game), score: create_attrs()

      assert %{id: _} = redirected_params(conn)
      assert redirected_to(conn) == twilight_imperium_game_path(conn, :show, game)

      conn = get conn, twilight_imperium_game_path(conn, :show, game)
      assert html_response(conn, 200) =~ "Show Game"
    end

    test "renders errors when data is invalid", %{conn: conn, game: game} do
      conn = post conn, twilight_imperium_game_score_path(conn, :create, game), score: @invalid_attrs
      assert html_response(conn, 200) =~ "New Victory Point Score"
    end
  end

  describe "edit score" do
    setup [:login_user, :create_game, :create_score]

    test "renders form for editing chosen score", %{conn: conn, game: game, score: score} do
      conn = get conn, twilight_imperium_game_score_path(conn, :edit, game, score)
      assert html_response(conn, 200) =~ "Edit Victory Point Score"
    end
  end

  describe "update score" do
    setup [:login_user, :create_game, :create_score]

    test "redirects when data is valid", %{conn: conn, game: game, score: score} do
      conn = put conn, twilight_imperium_game_score_path(conn, :update, game, score), score: @update_attrs
      assert redirected_to(conn) == twilight_imperium_game_path(conn, :show, game)

      conn = get conn, twilight_imperium_game_path(conn, :show, game)
      assert html_response(conn, 200)
    end

    test "renders errors when data is invalid", %{conn: conn, game: game, score: score} do
      conn = put conn, twilight_imperium_game_score_path(conn, :update, game, score), score: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Victory Point Score"
    end
  end

  describe "delete score" do
    setup [:login_user, :create_game, :create_score]

    test "deletes chosen score", %{conn: conn, game: game, score: score} do
      conn = delete conn, twilight_imperium_game_score_path(conn, :delete, game, score)
      assert redirected_to(conn) == twilight_imperium_game_path(conn, :show, game)
    end
  end

  defp create_game(%{conn: conn}) do
    game = fixture(:game, current_user(conn).id)
    {:ok, game: game}
  end

  defp create_score(%{game: game}) do
    score = fixture(:score, game)
    {:ok, score: score}
  end

  defp login_user(_) do
    login(false)
  end
end
