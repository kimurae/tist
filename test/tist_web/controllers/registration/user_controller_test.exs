defmodule TistWeb.Registration.UserControllerTest do
  use TistWeb.ConnCase

  @create_attrs   %{login: "some login", name: "some name", password: "some password_hash"}
  @invalid_attrs  %{login: nil, name: nil, password: nil}

  describe "new user" do
    test "renders form", %{conn: conn} do
      conn = get conn, registration_user_path(conn, :new)
      assert html_response(conn, 200) =~ "Register"
    end
  end

  describe "create user" do
    test "redirects to root when data is valid", %{conn: conn} do
      conn = post conn, registration_user_path(conn, :create), user: @create_attrs

      assert redirected_to(conn) == "/"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, registration_user_path(conn, :create), user: @invalid_attrs
      assert html_response(conn, 200) =~ "Register"
    end
  end
end
