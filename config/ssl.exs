use Mix.Config

# Forces the app to use an SSL endpoint.
config :tist, TistWeb.Endpoint,
  force_ssl: [rewrite_on: [:x_forwarded_proto]],
  url:       [scheme: "https", port: 443]
