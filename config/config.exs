# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :tist,
  ecto_repos: [Tist.Repo]

# Configures the endpoint
config :tist, TistWeb.Endpoint, pubsub: [
  adapter: Phoenix.PubSub.PG2,
  name:    Tist.PubSub
]

config :tist, TistWeb.Endpoint, render_errors: [
  accepts: ~w(html json),
  view:    TistWeb.ErrorView
]

config :tist, TistWeb.Endpoint,
  secret_key_base: "xPkUUuchvxfg7hRRzMVbcWcBnULtnq7mB5CoMeeXHNpU3BGlebuo+VRJUI0UURtG"

config :tist, TistWeb.Endpoint, url: [
  host: System.get_env() |> Map.get("HOST", "localhost")
]

# Configures Elixir's Logger
config :logger, :console,
  format:   "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :tist, Tist.Repo, adapter: Ecto.Adapters.Postgres

config :tist, Tist.Repo, pool_size: System.get_env()
  |> Map.get("POOL_SIZE", "10")
  |> String.to_integer

config :tist, Tist.Authorization.Guardian,
  issuer:     "tist",
  secret_key: "dUppA1j0H2Mx0hrhK7ZT7E17/HuyRPE9dz7kg6ABjQx93eK8+C3g6e8oQBuR7h1R"

if System.get_env("FORCE_SSL") == "true", do: import_config "ssl.exs"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
