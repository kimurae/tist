use Mix.Config

# Heroku configuration
config :tist, TistWeb.Endpoint,
  cache_static_manifest: "priv/static/cache_manifest.json",
  load_from_system_env:   true,
  secret_key_base:        System.get_env() |> Map.fetch!("SECRET_KEY_BASE")

config :tist, Tist.Repo, ssl: true

config :tist, Tist.Authorization.Guardian,
  secret_key: System.get_env() |> Map.fetch!("GUARDIAN_SECRET_KEY_BASE")
