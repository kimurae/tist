defmodule Tist.Authorization.Pipeline do
  use Guardian.Plug.Pipeline, otp_app: :auth_ex,
    error_handler:  Tist.Authorization.ErrorHandler,
    module:         Tist.Authorization.Guardian

  plug Guardian.Plug.VerifySession, claims: %{"typ" => "access"}
  plug Guardian.Plug.VerifyHeader, claims: %{"typ" => "access"}
  plug Guardian.Plug.LoadResource, allow_blank: true
end
