defmodule Tist.Authorization.CurrentUser do

  def authenticated?(conn) do
    Tist.Authorization.Guardian.Plug.authenticated?(conn)
  end

  def current_user(conn) do
    Tist.Authorization.Guardian.Plug.current_resource(conn)
  end

  def current_user_id(conn), do: current_user(conn).id
end
