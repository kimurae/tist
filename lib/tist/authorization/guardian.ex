defmodule Tist.Authorization.Guardian do
  use Guardian, otp_app: :tist,
    permissions: %{
      admin: [:approve_users]
    }

  use Guardian.Permissions.Bitwise

  alias Tist.Authorization
  alias Tist.Authorization.User

  def build_claims(claims, user, opts) do
    claims =
      claims
      |> encode_permissions_into_claims!(Keyword.get(opts, :permissions))
      |> claims_from_user(user)

    {:ok, claims}
  end

  def subject_for_token(%User{approved: true} = resource, _claims) do
    {:ok, to_string(resource.id)}
  end

  def resource_from_claims(%{"sub" => user_id} = _claims) do
    {:ok, Authorization.get_user!(user_id) }
  end

  defp claims_from_user(claims, %User{admin: true}) do
    claims
    |> encode_permissions_into_claims!(%{admin: [:approve_users]})
  end

  defp claims_from_user(claims, %User{admin: false}) do
    claims
  end
end
