defmodule Tist.Authorization.User do
  use Ecto.Schema

  import Ecto.Query

  alias Tist.Authorization.User

  schema "users" do
    field :admin,         :boolean, default: false
    field :approved,      :boolean, default: false
    field :login,         :string
    field :name,          :string
    field :password_hash, :string

    timestamps()
  end

  def approved(query \\ User) do
    query
    |> where([u], u.approved == true)
  end

  def by_login(query \\ User, login) do
    query
    |> where([u], u.login == ^login)
  end
end
