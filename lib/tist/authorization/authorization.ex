defmodule Tist.Authorization do
  @moduledoc """
  The Authorization context.
  """

  alias Comeonin.Bcrypt
  alias Tist.Authorization.User
  alias Tist.Repo

  @doc """
  Authorizes a user by login and password.

  ## Examples

    iex> authenticate_user(session_params)
    {:ok, %User{}}

    iex> authenticate_user(session_params)
    {:error, "Invalid login or password."}

  """
  def authenticate_user(%{"login" => login, "password" => password}) do
    login
    |> get_user_by_login()
    |> check_password(password)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id) do
    User.approved
    |> Repo.get!(id)
  end

  defp check_password(%User{} = user, password) do
    case Bcrypt.checkpw(password, user.password_hash) do
      true ->
        {:ok, user}
      false ->
        check_password(nil,nil)
    end
  end

  defp check_password(_, _), do: {:error, "Invalid login or password"}

  defp get_user_by_login(login) do
    User.approved
    |> User.by_login(login)
    |> Repo.one()
  end
end
