defmodule Tist.TwilightImperium.Score do
  use Ecto.Schema
  import Ecto.Changeset

  alias Tist.TwilightImperium
  alias Tist.TwilightImperium.{Faction, Game, Score}

  schema "scores" do
    belongs_to  :faction, Faction
    belongs_to  :game,    Game

    field :score, :integer

    timestamps()
  end

  def changeset(%Score{} = score, attrs) do
    score
    |> cast(attrs, ~w(faction_id score)a)
    |> foreign_key_constraint(:faction_id)
    |> validate_required(:score)
  end
end
