defmodule Tist.TwilightImperium.Statistic do
  use Ecto.Schema

  @primary_key false

  # This is a materialized view, so no insertion of records please.
  schema "statistics_mv" do
    field :faction,         :string
    field :games,           :integer
    field :score_per_turn,  :decimal
    field :wins,            :integer
  end
end
