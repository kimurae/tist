defmodule Tist.TwilightImperium.Faction do
  use Ecto.Schema

  alias Tist.TwilightImperium.{Game, Score}

  schema "factions" do
    has_many :wins,   Game, foreign_key: :winning_faction_id
    has_many :scores, Score

    field :name, :string

    timestamps()
  end
end
