defmodule Tist.TwilightImperium.Game do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  alias Tist.TwilightImperium
  alias Tist.TwilightImperium.{Faction,Game,Score}

  schema "games" do
    belongs_to  :winning_faction, Faction
    belongs_to  :user,            Tist.Authorization.User
    has_many    :scores,          Score

    field :played_on,     :date
    field :player_count,  :integer
    field :turns,         :integer

    timestamps()
  end

  @doc false
  def changeset(%Game{} = game, attrs) do
    game
    |> cast(attrs, ~w(played_on player_count turns winning_faction_id)a)
    |> foreign_key_constraint(:winning_faction_id)
    |> validate_required(~w(played_on turns)a)
    |> validate_inclusion(:player_count, 3..6)
  end

  @doc false
  def owned_by(query \\ Game, user_id) do
    query
    |> where([g], g.user_id == ^user_id)
  end

end
