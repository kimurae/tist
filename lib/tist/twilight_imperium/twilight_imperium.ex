defmodule Tist.TwilightImperium do
  @moduledoc """
  The TwilightImperium context.
  """

  import Ecto.Query, warn: false
  alias Tist.Repo

  alias Tist.TwilightImperium.{Faction, Game, Score, Statistic}

  @doc """
  Returns the list of twilight_imperium_games.

  ## Examples

      iex> list_twilight_imperium_games()
      [%Game{}, ...]

  """
  def list_twilight_imperium_games(user_id) do
    Game
    |> Game.owned_by(user_id)
    |> Repo.all()
    |> Repo.preload(:winning_faction)
  end

  @doc """
  Returns a list of statistics.

  ## Example

    iex> list_statistics()
    [%Statistic{}]

  """
  def list_statistics do
    Statistic
    |> order_by(asc: :faction)
    |> Repo.all()
  end

  @doc """
  Gets a single game.

  Raises `Ecto.NoResultsError` if the Game does not exist.

  ## Examples

      iex> get_game!(1,123,:scores)
      %Game{}

      iex> get_game!(1,456,:scores)
      ** (Ecto.NoResultsError)

  """
  def get_game!(user_id, id, preloads \\ []) do
    Game
    |> Game.owned_by(user_id)
    |> Repo.get!(id)
    |> Repo.preload(preloads)
    #|> (&Enum.reduce(preloads, &1, &Repo.preload/2)).()
  end

  def get_game!(user_id), do: %Game{user_id: user_id}

  @doc """
  Gets a single score.

  Raises `Ecto.NoResultsError` if the Game does not exist.

  ## Examples

      iex> get_score(game,123)
      %Game{}

      iex> get_score(game,456)
      ** (Ecto.NoResultsError)
  """
  def get_score!(game, id) do
    game
    |> Ecto.assoc(:scores)
    |> Repo.get!(id)
  end

  def get_score!(game), do: %Score{game_id: game.id}

  @doc """
  Creates a game.

  ## Examples

      iex> create_game(%{field: value})
      {:ok, %Game{}}

      iex> create_game(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_game(user_id, attrs \\ %{}) do
    %Game{user_id: user_id}
    |> Game.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Creates a score.

  ## Examples

      iex> create_score(%{field: value})
      {:ok, %Score{}}

      iex> create_score(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_score(game, attrs \\ %{}) do
    game
    |> Ecto.build_assoc(:scores)
    |> Score.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a game.

  ## Examples

      iex> update_game(game, %{field: new_value})
      {:ok, %Game{}}

      iex> update_game(game, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_game(%Game{} = game, attrs) do
    game
    |> Game.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Updates a score.

  ## Examples

      iex> update_score(score, %{field: new_value})
      {:ok, %Score{}}

      iex> update_score(score, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_score(%Score{} = score, attrs) do
    score
    |> Score.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Game.

  ## Examples

      iex> delete_game(game)
      {:ok, %Game{}}

      iex> delete_game(game)
      {:error, %Ecto.Changeset{}}

  """
  def delete_game(%Game{} = game) do
    Repo.delete(game)
  end

  @doc """
  Deletes a Score.

  ## Examples

      iex> delete_score(score)
      {:ok, %Score{}}

      iex> delete_score(score)
      {:error, %Ecto.Changeset{}}
  """
  def delete_score(%Score{} = score) do
    Repo.delete(score)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking game changes.

  ## Examples

      iex> change_game(game)
      %Ecto.Changeset{source: %Game{}}

  """
  def change_game(%Game{} = game) do
    Game.changeset(game, %{})
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking score changes.

  ## Examples

      iex> change_score(score)
      %Ecto.Changeset{source: %Score{}}

  """
  def change_score(%Score{} = score) do
    Score.changeset(score, %{})
  end

  @doc """
  Returns a list of factions for a select form element.

  ## Examples

    iex> faction_collection()
    %Map{['faction', 1]}

  """
  def faction_collection do
    Faction
    |> order_by(asc: :name)
    |> Repo.all()
    |> Enum.map(&{&1.name, &1.id})
    |> Enum.into([{"", nil}])
  end
end
