defmodule Tist.Admin.User do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  alias Tist.Admin.User

  schema "users" do
    field :approved, :boolean, default: false
    field :name,     :string
    timestamps()
  end

  @doc false
  def approval_changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:approved])
    |> validate_required([:approved])
  end

  @doc false
  def pending(queryable \\ User) do
    queryable
    |> where([u], u.approved == false)
  end
end
