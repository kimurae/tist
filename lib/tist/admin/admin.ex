defmodule Tist.Admin do
  @moduledoc """
  The Admin context.
  """

  import Ecto.Query, warn: false
  alias Tist.Repo

  alias Tist.Admin.User

  @doc """
  Approves a user.

  ## Examples

    iex> approve_user(good_id)
    {:ok, %User{}}

    iex> approve_user(bad_id)
    {:error, %Ecto.ChangeSet{}}
  """
  def approve_user(id) do
    User
    |> Repo.get!(id)
    |> User.approval_changeset(%{approved: true})
    |> Repo.update()
  end

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user_id)
      {:ok, %User{}}

      iex> delete_user(user_id)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(id) do
    User
    |> Repo.get!(id)
    |> Repo.delete()
  end
end
