defmodule Tist.Registration.User do
  use Ecto.Schema
  import Ecto.Changeset

  alias Comeonin.Bcrypt
  alias Tist.Registration.User

  schema "users" do
    field :login,         :string
    field :name,          :string
    field :password,      :string, virtual: true
    field :password_hash, :string

    timestamps()
  end

  @doc false
  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, ~w(name password login)a)
    |> validate_required(~w(name password login)a)
    |> unique_constraint(:login)
    |> unique_constraint(:name)
    |> hash_password()
  end


  defp hash_password(%Ecto.Changeset{valid?: true, changes: %{password: password}} = changeset) do
    changeset
    |> put_change(:password_hash, Bcrypt.hashpwsalt(password))
  end

  defp hash_password(changeset), do: changeset
end
