defmodule TistWeb.Router do
  use TistWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Tist.Authorization.Pipeline
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :authenticated do
    plug Guardian.Plug.EnsureAuthenticated
    plug Guardian.Plug.LoadResource
  end

  pipeline :only_admin do
    plug Guardian.Permissions.Bitwise, ensure: %{admin: [:approve_users]}
  end

  scope "/", TistWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  scope "/", TistWeb.Authorization, as: :authorization do
    pipe_through :browser

    resources "/sessions", SessionController, only: [:new, :create]
  end

  scope "/", TistWeb.Authorization, as: :authorization do
    pipe_through [:browser, :authenticated]

    resources "/sessions", SessionController, only: [:delete]
  end


  scope "/admin", TistWeb.Admin, as: :admin do
    pipe_through [:browser, :authenticated, :only_admin]

    resources "/users", UserController, only: ~w(index create update delete)a
  end

  scope "/registration", TistWeb.Registration, as: :registration do
    pipe_through :browser

    get   "/new", UserController, :new
    post  "/",    UserController, :create
  end

  scope "/twilight_imperium", TistWeb.TwilightImperium, as: :twilight_imperium do
    pipe_through [:browser, :authenticated]

    resources "/games", GameController do
      resources "/scores", ScoreController, except: ~w(index show)a
    end
  end
  # Other scopes may use custom stacks.
  # scope "/api", TistWeb do
  #   pipe_through :api
  # end
end
