defmodule TistWeb.Authorization.SessionController do
  use TistWeb, :controller

  alias Tist.Authorization

  def new(conn, _params) do
    conn
    |> render("new.html")
  end

  def create(conn, %{"session" => session_params}) do
    case Authorization.authenticate_user(session_params) do
      {:ok, user} ->
        conn
        |> Authorization.Guardian.Plug.sign_in(user, [])
        |> put_flash(:info, "Successful Login")
        |> redirect(to: "/")
      {:error, message} ->
        conn
        |> put_flash(:error, message)
        |> render("new.html")
    end
  end

  def delete(conn, _params) do
    conn
    |> Authorization.Guardian.Plug.sign_out #(Authorization.get_user!(id))
    |> redirect(to: "/")
  end
end
