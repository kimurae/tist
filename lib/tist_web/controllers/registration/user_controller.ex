defmodule TistWeb.Registration.UserController do
  use TistWeb, :controller

  alias Tist.Registration

  def new(conn, _params) do
    render(conn, "new.html", changeset: Registration.changeset)
  end

  def create(conn, %{"user" => user_params}) do
    case Registration.create(user_params) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Registration pending approval.")
        |> redirect(to: "/")
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end
end
