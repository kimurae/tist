defmodule TistWeb.TwilightImperium.GameController do
  use TistWeb, :controller

  alias Tist.TwilightImperium
  alias Tist.TwilightImperium.Game

  def index(conn, _params) do
    twilight_imperium_games = TwilightImperium.list_twilight_imperium_games(current_user(conn).id)
    render(conn, "index.html", twilight_imperium_games: twilight_imperium_games)
  end

  def new(conn, _params) do
    changeset = TwilightImperium.change_game(TwilightImperium.get_game!(current_user(conn).id))

    conn
    |> assign(:changeset, changeset)
    |> assign(:factions, TwilightImperium.faction_collection)
    |> render("new.html")
  end

  def create(conn, %{"game" => game_params}) do
    case TwilightImperium.create_game(current_user(conn).id, game_params) do
      {:ok, game} ->
        conn
        |> put_flash(:info, "Game created successfully.")
        |> redirect(to: twilight_imperium_game_path(conn, :show, game))
      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> assign(:changeset, changeset)
        |> assign(:factions, TwilightImperium.faction_collection)
        |> render("new.html")
    end
  end

  def show(conn, %{"id" => id}) do
    game = TwilightImperium.get_game!(current_user(conn).id, id, [:winning_faction, scores: :faction])
    render(conn, "show.html", game: game)
  end

  def edit(conn, %{"id" => id}) do
    game = TwilightImperium.get_game!(current_user(conn).id, id)
    changeset = TwilightImperium.change_game(game)

    conn
    |> assign(:changeset, TwilightImperium.change_game(game))
    |> assign(:factions,  TwilightImperium.faction_collection)
    |> assign(:game,      game)
    |> render("edit.html")
  end

  def update(conn, %{"id" => id, "game" => game_params}) do
    game = TwilightImperium.get_game!(current_user(conn).id, id)

    case TwilightImperium.update_game(game, game_params) do
      {:ok, game} ->
        conn
        |> put_flash(:info, "Game updated successfully.")
        |> redirect(to: twilight_imperium_game_path(conn, :show, game))
      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> assign(:changeset, changeset)
        |> assign(:factions,  TwilightImperium.faction_collection)
        |> assign(:game,      game)
        |> render("edit.html")
    end
  end

  def delete(conn, %{"id" => id}) do
    game = TwilightImperium.get_game!(current_user(conn).id, id)
    {:ok, _game} = TwilightImperium.delete_game(game)

    conn
    |> put_flash(:info, "Game deleted successfully.")
    |> redirect(to: twilight_imperium_game_path(conn, :index))
  end
end
