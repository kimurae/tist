defmodule TistWeb.TwilightImperium.ScoreController do
  use TistWeb, :controller

  alias Tist.TwilightImperium
  alias Tist.TwilightImperium.Game

  def new(conn, _params, game) do
    changeset = game
      |> TwilightImperium.get_score!
      |> TwilightImperium.change_score

    conn
    |> assign(:changeset, changeset)
    |> assign(:game,      game)
    |> assign(:factions,  TwilightImperium.faction_collection)
    |> render("new.html")
  end

  def create(conn, %{"score" => score_params}, game) do
    case TwilightImperium.create_score(game, score_params) do
      {:ok, score} ->
        conn
        |> put_flash(:info, "Game created successfully.")
        |> redirect(to: twilight_imperium_game_path(conn, :show, game))
      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> assign(:changeset, changeset)
        |> assign(:game,      game)
        |> assign(:factions, TwilightImperium.faction_collection)
        |> render("new.html")
    end
  end

  def edit(conn, %{"id" => id}, game) do
    score = TwilightImperium.get_score!(game, id)

    conn
    |> assign(:changeset, TwilightImperium.change_score(score))
    |> assign(:factions,  TwilightImperium.faction_collection)
    |> assign(:game,      game)
    |> assign(:score,     score)
    |> render("edit.html")
  end

  def update(conn, %{"id" => id, "score" => score_params}, game) do
    score = TwilightImperium.get_score!(game, id)

    case TwilightImperium.update_score(score, score_params) do
      {:ok, score} ->
        conn
        |> put_flash(:info, "Game updated successfully.")
        |> redirect(to: twilight_imperium_game_path(conn, :show, game))
      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> assign(:changeset, changeset)
        |> assign(:factions,  TwilightImperium.faction_collection)
        |> assign(:game,      game)
        |> assign(:score,     score)
        |> render("edit.html")
    end
  end

  def delete(conn, %{"id" => id}, game) do
    score = TwilightImperium.get_score!(game, id)
    {:ok, _score} = TwilightImperium.delete_score(score)

    conn
    |> put_flash(:info, "Game deleted successfully.")
    |> redirect(to: twilight_imperium_game_path(conn, :show, game))
  end

  def action(conn, _) do
    game = conn
      |> current_user_id()
      |> TwilightImperium.get_game!(conn.params["game_id"])

    apply(__MODULE__, action_name(conn), [conn, conn.params, game])
  end
end
