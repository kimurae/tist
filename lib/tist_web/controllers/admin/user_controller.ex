defmodule TistWeb.Admin.UserController do
  use TistWeb, :controller

  alias Tist.Admin

  def index(conn, _params) do
    conn
    |> assign(:users, Admin.list_users())
    |> render("index.html")
  end

  def update(conn, %{"id" => id}) do
    {:ok, _user} = Admin.approve_user(id)

    conn
    |> put_flash(:info, "User approved.")
    |> redirect(to: admin_user_path(conn, :index))
  end

  def delete(conn, %{"id" => id}) do
    {:ok, _user} = Admin.delete_user(id)

    conn
    |> put_flash(:info, "User deleted successfully.")
    |> redirect(to: admin_user_path(conn, :index))
  end
end
