defmodule TistWeb.PageController do
  use TistWeb, :controller

  def index(conn, _params) do
    conn
    |> assign(:statistics, Tist.TwilightImperium.list_statistics())
    |> render("index.html")
  end
end
