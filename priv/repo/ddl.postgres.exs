alias Ecto.Adapters.SQL
alias Tist.Repo

{:ok, _} = SQL.query(Repo, "DROP TRIGGER IF EXISTS factions_refresh_statistics_trigger ON factions;", [])
{:ok, _} = SQL.query(Repo, "DROP TRIGGER IF EXISTS games_refresh_statistics_trigger ON games;", [])
{:ok, _} = SQL.query(Repo, "DROP TRIGGER IF EXISTS scores_refresh_statistics_trigger ON scores;", [])

{:ok, _} = SQL.query(Repo, "DROP MATERIALIZED VIEW IF EXISTS statistics_mv CASCADE;", [])

{:ok, _} = SQL.query(Repo, "CREATE MATERIALIZED VIEW statistics_mv AS
     SELECT f.name AS faction,
            COUNT(gw.id) AS wins,
            ROUND( SUM(s.score::numeric) / SUM(gp.turns::numeric), 3) AS score_per_turn,
            COUNT(gp.id) AS games
       FROM factions f
  LEFT JOIN games gw ON f.id  = gw.winning_faction_id
  LEFT JOIN scores s ON f.id  = s.faction_id
  LEFT JOIN games gp ON gp.id = s.game_id
   GROUP BY f.name;", [])

{:ok, _} = SQL.query(Repo, "CREATE OR REPLACE FUNCTION refresh_statistics_mv() RETURNS TRIGGER LANGUAGE plpgsql AS $$
BEGIN
    REFRESH MATERIALIZED VIEW statistics_mv;
    RETURN null;
END $$;", [])

{:ok, _} = SQL.query(Repo, "CREATE TRIGGER factions_refresh_statistics_trigger
            AFTER insert
               OR update
               OR delete
               OR truncate
               ON factions FOR EACH STATEMENT
EXECUTE PROCEDURE refresh_statistics_mv();", [])

{:ok, _} = SQL.query(Repo, "CREATE TRIGGER games_refresh_statistics_trigger
            AFTER insert
               OR update
               OR delete
               OR truncate
               ON games FOR EACH STATEMENT
EXECUTE PROCEDURE refresh_statistics_mv();", [])

{:ok, _} = SQL.query(Repo, "CREATE TRIGGER scores_refresh_statistics_trigger
            AFTER insert
               OR update
               OR delete
               OR truncate
               ON scores FOR EACH STATEMENT
EXECUTE PROCEDURE refresh_statistics_mv();", [])

