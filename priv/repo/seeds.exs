# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Tist.Repo.insert!(%Tist.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

factions = for x <- [
    "The Emirates of Hacan",
    "The Federation of Sol",
    "The Barony of Letnev",
    "The Xxcha",
    "The Universities of Jol Nar",
    "Sardakk N'orr",
    "The L1Z1X Mindset",
    "The Naalu Collective",
    "The Mentak Coalition",
    "The Yssaril Tribes",
    "The Winnu",
    "The Embers of Muatt",
    "The Yin Brotherhood",
    "The Clan of Saar",
    "The Ghosts of Creuss",
    "The Aborec",
    "The Nekro Virus"
  ], nil == Tist.Repo.get_by(Tist.TwilightImperium.Faction, name: x), into: [], do: %Tist.TwilightImperium.Faction{name: x}

for faction <- factions, do: Tist.Repo.insert!(faction)
