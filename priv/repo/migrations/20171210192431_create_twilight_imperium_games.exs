defmodule Tist.Repo.Migrations.CreateTwilightImperiumGames do
  use Ecto.Migration

  def change do
    create table(:factions) do
      add :name, :string, null: false

      timestamps()
    end

    create table(:games) do
      add :played_on,     :date
      add :player_count,  :integer, null: false, default: 0
      add :turns,         :integer, null: false, default: 1

      add :user_id,             references(:users,    on_delete: :restrict)
      add :winning_faction_id,  references(:factions, on_delete: :restrict)
      timestamps()
    end

    create table(:scores) do
      add :score, :integer, null: false, default: 0

      add :game_id,     references(:games,    on_delete: :delete_all)
      add :faction_id,  references(:factions, on_delete: :restrict)
      timestamps()
    end

    create unique_index(:scores, [:game_id, :faction_id])
  end
end
