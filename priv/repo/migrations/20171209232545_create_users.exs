defmodule Tist.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :admin,         :boolean, null: false, default: false
      add :approved,      :boolean, null: false, default: false
      add :login,         :string,  null: false
      add :name,          :string,  null: false
      add :password_hash, :string,  null: false

      timestamps()
    end

    create unique_index(:users, [:login])
    create unique_index(:users, [:name])
  end
end
