# Tist
A Twilight Imperium Statistics Tracker

## Running

### Environment Variables
In production the following environment variables can be used to override the
default local settings:

* DATABASE_URL, if present it overrides the dabatabse settings, by default it will connect to localhost using the logged in user.
* FORCE_SSL, defaults to false, if set to true, sets the app to only accept SSL connections.
* GUARDIAN_SECRET_KEY_BASE, you want to set this on production instances.
* HOST, defaults to localhost
* MIX_ENV, defaults to dev
* POOL_SIZE, number of db connections, defaults to 10.
* SECRET_KEY_BASE, you want to set this on production instances.

### Dependencies
* [Pheonix and Elixr](http://www.phoenixframework.org/)
* Postgres (I use [Postgres.app](www.postgresapp.com)

### Local setup
  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `cd assets && npm install`
  * Seed faction data with `mix run priv/repo/seeds.exs`
  * Generate Custom Postgres DDLs with `mix run priv/repo/ddl.postgres.exs`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Learn more

### Phoenix
  * [Official website:](http://www.phoenixframework.org/)
  * [Guides:](http://phoenixframework.org/docs/overview)
  * [Docs:](https://hexdocs.pm/phoenix)
  * [Mailing list:](http://groups.google.com/group/phoenix-talk)
  * [Source:](https://github.com/phoenixframework/phoenix)

### Twilight Imperium
  * [Fantasy Flight Games](https://www.fantasyflightgames.com/en/products/twilight-imperium-fourth-edition/)
